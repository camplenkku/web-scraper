import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TokopediaProductScraper {
    private static final String CSV_FILE_NAME = "products.csv";
    private static String baseUrl = "https://www.tokopedia.com/search?sc=24&st=product&page=";
    private WebDriver driver;
    public TokopediaProductScraper() {

    }

    private WebDriver getDriverInstance(){
        if (driver == null){
            System.setProperty("webdriver.chrome.driver","C://Users//zeazio//Documents//chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized");
            options.addArguments("--disable-gpu");
            options.addArguments("--disable-extensions");
            options.addArguments("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36");
            options.setHeadless(true);

            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }
        return driver;
    }

    public List<Product> scrapeData(int rowNums){
        System.out.println("Start to scrap " + rowNums + " data");
        List<String> urlProducts = new ArrayList<>();
        int page = 1;
        int diffRowNums = rowNums;
        while (urlProducts.size() < rowNums){
            String url = baseUrl + page;
            try {
                List<String> urlProductsTemp = findProductUrlList(url, diffRowNums);
                urlProducts.addAll(urlProductsTemp);
                page++;
                diffRowNums = rowNums - urlProducts.size();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        List<Product> products = urlProducts.stream().map(this::findDetailProducts).collect(Collectors.toList());
        if (driver != null){
            driver.close();
        }
        return products;
    }

    public void scrapeDataThenWriteToCsv(int rowNums) throws IOException {
        List<Product> products = scrapeData(rowNums);
        List<String[]> csvData = convertToCSV(products);
        System.out.println("Write to CSV");
        try (ICSVWriter writer = new CSVWriterBuilder(
                new FileWriter(CSV_FILE_NAME))
                .withSeparator(';')
                .build()) {
            writer.writeAll(csvData);
        }
    }

    private static List<String[]> convertToCSV(List<Product> products) {
        List<String[]> list = new ArrayList<>();
        String[] header = {"Url", "Product Name", "Product Description", "Product Image Links", "Product Price", "Product Rating", "Merchant Name"};
        list.add(header);
        products.forEach(product->{
            String concateProductImageLinks = product.getImageLinks().stream().collect(Collectors.joining(","));
            String[] dt = {product.getUrl(), product.getName(),
                           product.getDescription(), concateProductImageLinks,
                           product.getPrice().toString(), product.getRating(), product.getMerchantName()};
            list.add(dt);
        });
        return list;
    }

    private List<String> findProductUrlList(String url, int rowNums) throws InterruptedException {
        System.out.println("Start to extract url products -> url " + url);
        List<String> urlProducts = new ArrayList<>();
        driver = getDriverInstance();
        driver.get(url);
        //Thread.sleep(1000);
        boolean isPageLoaded = false;
        long initHeight = (Long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");
        initHeight=initHeight + initHeight;
        while (!isPageLoaded) {
            ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, " + initHeight + ");");
//            Thread.sleep(2000);
            initHeight  += initHeight;
            long pageHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");
            if (initHeight > pageHeight){
                isPageLoaded = true;
            }
        }

        final List<WebElement> webElements = driver.findElements(By.xpath("//div[@data-testid='divSRPContentProducts']//div[@data-testid='master-product-card']"));
        int counter=1;
        for (WebElement webElement : webElements){
            WebElement imgProduct = webElement.findElement(By.xpath(".//div[@data-testid='imgSRPProdMain']"));
            if (imgProduct != null){
                WebElement urlDetailProduct = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;", imgProduct);
                urlProducts.add(urlDetailProduct.getAttribute("href"));
            }
            if (counter == rowNums){
                System.out.println("end extract url products -> num of url " + urlProducts.size());
                return urlProducts;
            }
            counter++;
        }
        System.out.println("end extract url products -> num of url " + urlProducts.size());
        return urlProducts;
    }

    private Product findDetailProducts(String url){
        System.out.println("Start to extract detail product -> url " + url);
        driver = getDriverInstance();
        driver.get(url);
        WebElement detailProductElement = driver.findElement(By.xpath("//div[@id='main-pdp-container']"));
        Product product = Product.builder()
                .url(url)
                .build();
        if (detailProductElement != null){
            WebElement productContentDiv = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-product_content']"));
            WebElement productNameDiv = productContentDiv.findElement(By.xpath(".//h1[@data-testid='lblPDPDetailProductName']"));
            product.setName(productNameDiv != null && productNameDiv.getText() != null ? productNameDiv.getText() : "-");

            WebElement ratingElement = productContentDiv.findElement(By.xpath(".//span[@data-testid='lblPDPDetailProductRatingNumber']"));
            product.setRating(ratingElement != null ? ratingElement.getText() : "0.0");

            WebElement priceElement = productContentDiv.findElement(By.xpath(".//div[@data-testid='lblPDPDetailProductPrice']"));
            String itemPrice = ratingElement != null ? priceElement.getText() : "0.0";
            itemPrice = itemPrice.replace(".","").replace("Rp","");
            product.setPrice(new BigDecimal(itemPrice));

            WebElement merchantElement = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-shop_credibility']//a[@data-testid='llbPDPFooterShopName']"));
            product.setMerchantName(merchantElement != null ? merchantElement.getText() : "-");

            WebElement productDescriptionDiv = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-product_detail']//div[@data-testid='lblPDPDescriptionProduk']"));
            product.setDescription(productDescriptionDiv != null && productDescriptionDiv.getText() != null ? productDescriptionDiv.getText() : "-");

            List<WebElement> imageLinkElements = detailProductElement.findElements(By.xpath(".//div[@id='pdp_comp-product_media']//div[@data-testid='PDPImageThumbnail']/div/img"));
            List<String> imgUrls = imageLinkElements.stream().map(image -> image.getAttribute("src")).collect(Collectors.toList());
            product.setImageLinks(imgUrls);
        }
        System.out.println("end of extract product detailed");
        return product;
    }
}
