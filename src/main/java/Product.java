import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private String url;
    private String name;
    private String description;
    private List<String> imageLinks;
    private BigDecimal price;
    private String rating;
    private String merchantName;
}