import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        TokopediaProductScraper scraper = new TokopediaProductScraper();
        scraper.scrapeDataThenWriteToCsv(100);
    }
}
