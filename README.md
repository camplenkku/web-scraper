Libraries : selenium-java, opencsv

**Goal** :
1. get product list of _handphone category_ with each product contain information :
    ```
    Name of Product.
    Description.
    Image Link.
    Price.
    Rating(out of 5 stars).
    Name of store or merchant.
    ```

2. Save the products to CSV File

**Process** : 
1. Find the right and specific url page from tokopedia which serve product list of Handphone category
   Base url found in "https://www.tokopedia.com/search?sc=24&st=product&page=";
     
2. Gather info and try to interact the page how the data displayed
   - Found that every url hit, the page will be paginate by scroll.
   - Found that after scroll, whole product will be displayed for 60 item.
   - If I want to gather morethan 60 items, I have to iterate hit page until number of item is reached.  
   - Information of this page (baseUrl page) only have information : ProductName, ProductPrice, Rating, ImageUrl. There is no information about ProductDescription, MerchantName. So I have to try hit another page to get the detail.
   - Found in baseUrl page have link/url to detailPage.

3. Scrap Base url and map into List of String consists of url products
4. Every item in url products hit into detail page and map the result into List of Product object
5. Save into csv of list of product

**Mapper Search Product Page/BaseUrl**:
1. Getting product rows
   driver.findElements(By.xpath("//div[@data-testid='divSRPContentProducts']//div[@data-testid='master-product-card']"));
2. Getting url page of product 
   WebElement imgProduct = webElement.findElement(By.xpath(".//div[@data-testid='imgSRPProdMain']"));
   if (imgProduct != null) {
       WebElement urlDetailProduct = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;", imgProduct);
       urlProducts.add(urlDetailProduct.getAttribute("href"));
   }   

**Mapper of Product Detail Page**:
1. Get root of product element  
    `WebElement detailProductElement = driver.findElement(By.xpath("//div[@id='main-pdp-container']"));`

2. Get Product Name
    ```
    WebElement productContentDiv = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-product_content']"));
    WebElement productNameDiv = productContentDiv.findElement(By.xpath(".//h1[@data-testid='lblPDPDetailProductName']"));
    ```
   
3. Get Rating 
    ```
    WebElement ratingElement = productContentDiv.findElement(By.xpath(".//span[@data-testid='lblPDPDetailProductRatingNumber']"));
    ```
   
4. Get Price
    ```
    WebElement priceElement = productContentDiv.findElement(By.xpath(".//div[@data-testid='lblPDPDetailProductPrice']"));
    ```
   
5. Get MerchantName
    ```
    WebElement merchantElement = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-shop_credibility']//a[@data-testid='llbPDPFooterShopName']"));
    ```
   
6. Get Description
    ```
    WebElement productDescriptionDiv = detailProductElement.findElement(By.xpath(".//div[@id='pdp_comp-product_detail']//div[@data-testid='lblPDPDescriptionProduk']"));
    ```

7. Get Image Link
    ```
    List<WebElement> imageLinkElements = detailProductElement.findElements(By.xpath(".//div[@id='pdp_comp-product_media']//div[@data-testid='PDPImageThumbnail']/div/img"));
    ```
